#include <boost/core/lightweight_test.hpp>

#include <unitx/io/input.hpp>

#include <boost/units/systems/si.hpp>

using namespace boost::units;

void test_input_unit_symbol() { // base units using default symbol_format (no
                                // format specified) and no auto prefixing.
                                // #define FORMATTERS
  {
    quantity<si::length> l = 3.14 * si::meter;
    std::stringstream ss;
    ss << l << "abc" << std::endl;

    quantity<si::length> l2;
    ss >> l2;
    BOOST_TEST(ss);
    BOOST_TEST_EQ(l, l2);

    std::string check;
    ss >> check;
    BOOST_TEST_EQ(check, "abc");
  }

  // BOOST_UNITS_TEST_OUTPUT(meter_base_unit::unit_type(), "m");
  // BOOST_UNITS_TEST_OUTPUT(velocity(), "m s^-1");
  // BOOST_UNITS_TEST_OUTPUT(scaled_length(), "km");
  // BOOST_UNITS_TEST_OUTPUT(scaled_velocity1(), "k(m s^-1)");
  // BOOST_UNITS_TEST_OUTPUT(millisecond_base_unit::unit_type(), "ms");
  // BOOST_UNITS_TEST_OUTPUT(scaled_time(), "ms");
  // BOOST_UNITS_TEST_OUTPUT(scaled_velocity2(), "m ms^-1");
  // BOOST_UNITS_TEST_OUTPUT(area(), "m^2");
  // BOOST_UNITS_TEST_OUTPUT(scaled_area(), "k(m^2)");
  // BOOST_UNITS_TEST_OUTPUT(double_scaled_length(), "Kikm");
  // BOOST_UNITS_TEST_OUTPUT(double_scaled_length2(), "kscm");
  // BOOST_UNITS_TEST_OUTPUT(custom1(), "c1");
  // BOOST_UNITS_TEST_OUTPUT(custom2(), "c2");
  // BOOST_UNITS_TEST_OUTPUT(scaled_custom1(), "kc1");
  // BOOST_UNITS_TEST_OUTPUT(scaled_custom2(), "kc2");
  // BOOST_UNITS_TEST_OUTPUT(boost::units::absolute<meter_base_unit::unit_type>(),
  // "absolute m");
  // #undef FORMATTERS
}

int main() {
  test_input_unit_symbol();
  // test_input_unit_raw();
  // test_input_unit_name();
  // test_input_quantity_symbol();
  // test_input_quantity_raw();
  // test_input_quantity_name();
  // test_input_autoprefixed_quantity_name();
  // test_input_autoprefixed_quantity_symbol();
  // test_input_auto_binary_prefixed_quantity_symbol();
  // test_input_auto_binary_prefixed_quantity_name();
  // test_input_quantity_name_duplicate();
  // test_input_quantity_symbol_duplicate();
  // test_input_auto_binary_prefixed_quantity_name_duplicate();
  // test_input_auto_binary_prefixed_quantity_symbol_duplicate();
  // test_input_typename_format();
  return boost::report_errors();
}