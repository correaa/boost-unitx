#include <boost/core/lightweight_test.hpp>

#include "unitx/base_units/nonsi/electronvolt.hpp"

#include <unitx/systems/au.hpp>
#include <unitx/systems/au/si_conversion.hpp>
#include <unitx/systems/au/io.hpp>

#include <boost/units/io.hpp>
#include <boost/units/systems/si.hpp>

using namespace boost::units;

void test_nonsi_electronvolt() {
  {
    std::ostringstream ss;
    ss 
      << 1.1 * nonsi::electronvolt << '\n'
      // << 2.2 * nonsi::electron_volt << '\n'
      << 3.3 * nonsi::eV << '\n'
    ;

    BOOST_TEST_EQ(ss.str(), 
      "1.1 eV\n"
    // "2.2 eV\n"
      "3.3 eV\n"
    );
  }
  
  BOOST_TEST_EQ( (1.2*au::elementary_charge*si::volt / (1.2*nonsi::electronvolt)).value() , 1.0 );
  BOOST_TEST_EQ( quantity<si::dimensionless>(1.2*au::elementary_charge*si::volt / (1.2*nonsi::electronvolt)).value() , 1.0 );

  std::cerr << 1.2*au::elementary_charge*si::volt / (1.2*nonsi::electronvolt) << std::endl;
}

int main() {
  test_nonsi_electronvolt();
  return boost::report_errors();
}
