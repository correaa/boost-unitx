// Boost.Units - A C++ library for zero-overhead dimensional analysis and 
// unit/quantity manipulation and conversion
//
// Copyright (C) 2013 Alfredo Correa
//
// Distributed under the Boost Software License, Version 1.0. (See
// accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#ifndef BOOST_UNITS_SYSTEMS_NONSI_DALTON_BASE_UNIT_HPP
#define BOOST_UNITS_SYSTEMS_NONSI_DALTON_BASE_UNIT_HPP

#include <string>

#include <boost/units/make_system.hpp>

#include <boost/units/config.hpp>
#include <boost/units/base_unit.hpp>
#include <boost/units/physical_dimensions/mass.hpp>
#include <boost/units/base_units/si/kilogram.hpp>
#include <boost/units/conversion.hpp>

namespace boost{
namespace units{
namespace nonsi {

struct dalton_base_unit :
    boost::units::base_unit<dalton_base_unit, mass_dimension, 898>
{
    static std::string name()       { return "dalton"; }
    static std::string symbol()     { return "amu"; }
};

typedef boost::units::make_system<dalton_base_unit>::type system_da;

/// unit typedefs
typedef unit<mass_dimension,system_da>    mass;

static const mass amu;
static const mass dalton;


} // nonsi
}}

BOOST_UNITS_DEFINE_CONVERSION_FACTOR(nonsi::dalton_base_unit,
                                     boost::units::si::kilogram_base_unit,
                                     double, 1.6605388628e-27);
BOOST_UNITS_DEFAULT_CONVERSION(
	nonsi::dalton_base_unit,
	boost::units::si::kilogram_base_unit
);
#endif // BOOST_UNITS_SYSTEMS_NONSI_DALTON_BASE_UNIT_HPP

