// Boost.Units - A C++ library for zero-overhead dimensional analysis and 
// unit/quantity manipulation and conversion
//
// Copyright (C) 2013 Alfredo Correa
//
// Distributed under the Boost Software License, Version 1.0. (See
// accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#ifndef BOOST_UNITS_SYSTEMS_NONSI_RYDBERG_BASE_UNIT_HPP
#define BOOST_UNITS_SYSTEMS_NONSI_RYDBERG_BASE_UNIT_HPP

#include <string>

#include <boost/units/config.hpp>
#include <boost/units/base_unit.hpp>
#include <boost/units/physical_dimensions/energy.hpp>
//#include <boost/units/base_units/si/joule.hpp>
#include<boost/units/systems/si/energy.hpp>
#include <boost/units/conversion.hpp>
#if 0
BOOST_UNITS_DEFINE_BASE_UNIT_WITH_CONVERSIONS(nonsi, angstrom, "angstrom", "A", 1.e-10, si::meter_base_unit, -401);    // exact conversion
q
#if BOOST_UNITS_HAS_BOOST_TYPEOF

#include BOOST_TYPEOF_INCREMENT_REGISTRATION_GROUP()

BOOST_TYPEOF_REGISTER_TYPE(boost::units::nonsi::angstrom_base_unit)

#endif
#endif

namespace boost{
namespace units{
namespace nonsi {

struct rydberg_base_unit :
    boost::units::base_unit<rydberg_base_unit, energy_dimension, 995>
{
    static std::string name()       { return "rydberg"; }
    static std::string symbol()     { return "Ry"; }
};

typedef boost::units::make_system<rydberg_base_unit>::type system_ry;

/// unit typedefs
typedef unit<energy_dimension,system_ry>    energy_ry;

static const energy_ry rydberg, rydbergs, Ry;

} // nonsi
}}

// helper for conversions between nonsi energy and si energy
BOOST_UNITS_DEFINE_CONVERSION_FACTOR(nonsi::rydberg_base_unit,
                                    boost::units::si::energy,
                                    double, 2.18e-18);
BOOST_UNITS_DEFAULT_CONVERSION(
	nonsi::rydberg_base_unit,
	boost::units::si::energy
);
#endif // BOOST_UNITS_SYSTEMS_NONSI_RYDBERG_BASE_UNIT_HPP

