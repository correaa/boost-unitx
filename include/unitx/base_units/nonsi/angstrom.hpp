// Boost.Units - A C++ library for zero-overhead dimensional analysis and 
// unit/quantity manipulation and conversion
//
// Copyright (C) 2013-2023 Alfredo Correa
//
// Distributed under the Boost Software License, Version 1.0. (See
// accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#ifndef BOOST_UNITS_SYSTEMS_NONSI_ANGSTROM_BASE_UNIT_HPP
#define BOOST_UNITS_SYSTEMS_NONSI_ANGSTROM_BASE_UNIT_HPP

#include <string>

#include <boost/units/make_system.hpp>

#include <boost/units/config.hpp>
#include <boost/units/base_unit.hpp>
#include <boost/units/physical_dimensions/length.hpp>
#include <boost/units/physical_dimensions/volume.hpp>
#include <boost/units/systems/si.hpp>

#include <boost/units/systems/si/prefixes.hpp>

#include <boost/units/conversion.hpp>

#include "../../systems/au.hpp"

#if 0
BOOST_UNITS_DEFINE_BASE_UNIT_WITH_CONVERSIONS(nonsi, angstrom, "angstrom", "A", 1.e-10, si::meter_base_unit, -401);    // exact conversion

#if BOOST_UNITS_HAS_BOOST_TYPEOF

#include BOOST_TYPEOF_INCREMENT_REGISTRATION_GROUP()

BOOST_TYPEOF_REGISTER_TYPE(boost::units::nonsi::angstrom_base_unit)

#endif
#endif

namespace boost{
namespace units{
namespace nonsi{

struct angstrom_base_unit :
    boost::units::base_unit<angstrom_base_unit, length_dimension, 2>
{
    static std::string name()       { return "angstrom"; }
    static std::string symbol()     { return "AA"; }  // A is reserved for Ampere, consider Å
};

typedef boost::units::make_system<angstrom_base_unit>::type system;

/// unit typedefs
typedef unit<length_dimension,system>    length;
typedef unit<volume_dimension,system>    volume;

static const length angstrom, AA, Å;
static const volume AA3;

} // nonsi
}}

// helper for conversions between imperial length and si length
BOOST_UNITS_DEFINE_CONVERSION_FACTOR(nonsi::angstrom_base_unit,
                                     boost::units::si::meter_base_unit,
                                     double, 1.0e-10);
BOOST_UNITS_DEFAULT_CONVERSION(
	nonsi::angstrom_base_unit,
	boost::units::si::meter_base_unit
);

namespace boost {

namespace units { 

inline std::string name_string  (const reduce_unit<decltype(nonsi::A3/au::atom)>::type&) { return "angstrom cubed per atom"; }
inline std::string symbol_string(const reduce_unit<decltype(nonsi::A3/au::atom)>::type&) { return "AA^3/atom"; }

inline std::string name_string  (reduce_unit<decltype(nonsi::A/(si::femto*si::second))>::type) { return "angstrom per femtosecond"; }
inline std::string symbol_string(reduce_unit<decltype(nonsi::A/(si::femto*si::second))>::type) { return "AA/fs"; }

}}

#endif // BOOST_UNITS_SYSTEMS_NONSI_ANGSTROM_BASE_UNIT_HPP
