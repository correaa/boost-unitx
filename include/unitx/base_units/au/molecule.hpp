#ifndef BOOST_UNITS_AU_MOLECULE_BASE_UNIT_HPP
#define BOOST_UNITS_AU_MOLECULE_BASE_UNIT_HPP

#include <string>

#include <boost/units/base_unit.hpp>
#include <boost/units/physical_dimensions/amount.hpp>
//#include "alf/boost/units/physical_dimensions/amount_atom.hpp"

#ifndef BOOST_UNITS_AU_MOLECULE_NAME
#define BOOST_UNITS_AU_MOLECULE_NAME "molecule" // "species" "atom"
#endif
#ifndef BOOST_UNITS_AU_MOLECULE_SYMBOL
#define BOOST_UNITS_AU_MOLECULE_SYMBOL "molec" // "spec" "at"
#endif

namespace boost {

namespace units {

namespace au {

struct molecule_base_unit : base_unit<
	molecule_base_unit, 
	amount_dimension, 
	-17
>{
	static std::string name()   { return (text_name); }
	static std::string symbol() { return  (text_symbol); }

	static std::string text_name;
	static std::string text_symbol;
};

std::string molecule_base_unit::text_name = BOOST_UNITS_AU_SPECIES_NAME;
std::string molecule_base_unit::text_symbol = BOOST_UNITS_AU_SPECIES_SYMBOL;

} // namespace au

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_AU_MOLECULE_BASE_UNIT_HPP

