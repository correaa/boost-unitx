#ifndef BOOST_UNITS_AU_BOLTZMANN_CONSTANT_BASE_UNIT_HPP
#define BOOST_UNITS_AU_BOLTZMANN_CONSTANT_BASE_UNIT_HPP

#include <string>

#include <boost/units/base_unit.hpp>
#include <boost/units/physical_dimensions/heat_capacity.hpp>

namespace boost {

namespace units {

namespace au {

struct boltzmann_constant_base_unit : base_unit<
	boltzmann_constant_base_unit, heat_capacity_dimension,
	-12
>{
	static std::string name()   { return ("boltzmann_constant"); }
	static std::string symbol() { return ("k_B"); }
};

} // namespace au

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_AU_BOLTZMANN_CONSTANT_BASE_UNIT_HPP

