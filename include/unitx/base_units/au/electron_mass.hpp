#ifndef BOOST_UNITS_AU_ELECTRON_MASS_BASE_UNIT_HPP
#define BOOST_UNITS_AU_ELECTRON_MASS_BASE_UNIT_HPP

#include<string>
#include <boost/units/base_unit.hpp>
#include <boost/units/physical_dimensions/mass.hpp>

namespace boost {

namespace units {

namespace au {

struct electron_mass_base_unit : base_unit<
	electron_mass_base_unit, mass_dimension, 
	-14
>{
	static std::string name()   {return ("electron_mass"); }
	static std::string symbol() {return ("m_e"); }
};

} // namespace au

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_AU_ELECTRON_MASS_BASE_UNIT_HPP

