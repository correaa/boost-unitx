#ifndef BOOST_UNITS_AU_COULOMB_FORCE_CONSTANT_BASE_UNIT_HPP
#define BOOST_UNITS_AU_COULOMB_FORCE_CONSTANT_BASE_UNIT_HPP

#include <string>

#include <boost/units/base_unit.hpp>
#include "../../physical_dimensions/elastivity.hpp"

namespace boost {

namespace units {

namespace au {

struct coulomb_force_constant_base_unit : base_unit<
	coulomb_force_constant_base_unit, elastivity_dimension,
	-13
>{
	static std::string name(){ return (
	//	"Coulomb constant"	/* wolfram friendly */
		"coulombforceconstant"	/* wiki friendly */
	//	"ensuremath{k_e}"
		);
	}
	static std::string symbol() { return  (
		"k_e"					// wiki friendly
	//	"1/(4 pi epsilon_0)"	// wiki friendly
	//	"k_1"					// Jackson EM book friendly
	//	"kappa"					// wolfram friendly
	);
	}
};

} // namespace au

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_AU_COULOMB_FORCE_CONSTANT_BASE_UNIT_HPP

