#ifndef BOOST_UNITS_AU_BOHR_RADIUS_BASE_UNIT_HPP
#define BOOST_UNITS_AU_BOHR_RADIUS_BASE_UNIT_HPP

#include <string>

#include <boost/units/base_unit.hpp>
#include <boost/units/physical_dimensions/length.hpp>

namespace boost {

namespace units {

namespace au {

struct bohr_radius_base_unit : base_unit<
	bohr_radius_base_unit, length_dimension,
	-11
>{
	static std::string name()   { return ("bohr"); }
	static std::string symbol() { return  ("a_0"); }
};

} // namespace au

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_AU_BOHR_RADIUS_BASE_UNIT_HPP

