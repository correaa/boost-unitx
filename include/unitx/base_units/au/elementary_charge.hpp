#ifndef BOOST_UNITS_AU_ELEMENTARY_CHARGE_BASE_UNIT_HPP
#define BOOST_UNITS_AU_ELEMENTARY_CHARGE_BASE_UNIT_HPP

#include <string>

#include <boost/units/base_unit.hpp>
#include <boost/units/physical_dimensions/electric_charge.hpp>

namespace boost {

namespace units {

namespace au {

struct elementary_charge_base_unit : base_unit<
	elementary_charge_base_unit, electric_charge_dimension, //derived_dimension<time_base_dimension,1, current_base_dimension,1>::type,
	-15
>{
	static std::string name()   { return (
	//	"elementary charge"	/*wiki friendly*/
		"elementarycharge"	/*siunitx friendly*/
	//	"electron charge"	/*wolfram friendly*/
	);}
	static std::string symbol() { return ("e");}
};

} // namespace au

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_AU_ELEMENTARY_CHARGE_BASE_UNIT_HPP

