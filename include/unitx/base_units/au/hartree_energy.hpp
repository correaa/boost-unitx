#ifndef BOOST_UNITS_AU_HARTREE_ENERGY_BASE_UNIT_HPP
#define BOOST_UNITS_AU_HARTREE_ENERGY_BASE_UNIT_HPP

#include <string>

#include <boost/units/base_unit.hpp>
#include <boost/units/physical_dimensions/energy.hpp>

namespace boost {

namespace units {

namespace au {

struct hartree_energy_base_unit : base_unit<
	hartree_energy_base_unit, energy_dimension,
	-16
>{
	static std::string name()   { return (
	//	"hartree energy" // wiki friendly
		"hartree" // siunitx friendly
	); }
	static std::string symbol() { return  ("E_h"); }
};

} // namespace au

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_AU_HARTREE_ENERGY_BASE_UNIT_HPP

