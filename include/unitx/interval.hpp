#ifdef COMPILATION_INSTRUCTIONS
ln -sf $0 $0.cpp && time clang++ -std=c++17 -Wall -D_TEST_BOOST_UNITS_INTERVAL $0.cpp -o $0.x && $0.x $@; rm -f $0.cpp $0.x; exit
#endif
#ifndef BOOST_UNITS_INTERVAL_HPP
#define BOOST_UNITS_INTERVAL_HPP

#include<boost/numeric/interval.hpp>
#include<boost/numeric/interval/checking.hpp>

#include<boost/units/quantity.hpp>
#include<boost/units/limits.hpp>

namespace boost{
namespace numeric {
namespace interval_lib{

//! The checking policy for boost::interval, if it uses boost::unit
//  model code taken from boost/numeric/interval/checking.hpp
// looks like I have to repear the whole class here http://stackoverflow.com/a/1797539/225186
template<class Unit, class TT>
struct checking_base<boost::units::quantity<Unit, TT> >{
	private:
	typedef boost::units::quantity<Unit> T; //to minimize code change
	public:
	static constexpr T pos_inf(){
		static_assert(std::numeric_limits<T>::has_infinity, "");
		return std::numeric_limits<T>::infinity();
	}
	static constexpr T neg_inf(){
		static_assert(std::numeric_limits<T>::has_infinity, "");
		return -std::numeric_limits<T>::infinity();
	}
	static constexpr T nan(){
		static_assert(std::numeric_limits<T>::has_quiet_NaN, "");
		return std::numeric_limits<T>::quiet_NaN();
	}
	static constexpr bool is_nan(const T& x){
		static_assert(std::numeric_limits<T>::has_quiet_NaN, "");
		return x != x;
	}
	static constexpr T empty_lower(){
		static_assert(std::numeric_limits<T>::has_quiet_NaN, "");
		return std::numeric_limits<T>::quiet_NaN();
	}
	static constexpr T empty_upper(){
		static_assert(std::numeric_limits<T>::has_quiet_NaN, "");
		return std::numeric_limits<T>::quiet_NaN();
	}
	static constexpr bool is_empty(const T& l, const T& u){
		return !(l <= u); // safety for partial orders
	}
};

template<class U, class T>
interval<T> const& value(interval<units::quantity<U, T>> const& iv){
	return reinterpret_cast<interval<T> const&>(iv);
}
template<class D, class S, class T>
decltype(auto) operator*(interval<T> const& iv, units::unit<D, S> u){
	return reinterpret_cast<interval<decltype(lower(iv)*u)> const&>(iv);
}

}}}

#ifdef  _TEST_BOOST_UNITS_INTERVAL

#include<boost/units/systems/si/io.hpp>
#include<boost/numeric/interval/io.hpp>
#include<iostream>

using namespace boost::units;
using std::clog;

int main(){
	using boost::numeric::interval;

	interval iv{1.*si::meter, 10.*si::meter};
	clog 
		<< iv << '\n'
		<< value(iv) << '\n'
	;
	auto iv2 = interval{1., 10.}*si::meter;
	clog << iv2 << '\n';
}
#endif
#endif

