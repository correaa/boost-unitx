#ifdef compile_instructions
(echo "#include\""$0"\"" > $0x.cpp) && c++ -std=c++11 -Wfatal-errors -D_TEST_BOOST_UNITS_COMPARISON $0x.cpp -o $0x.x && $0x.x $@; rm $0x.x $0x.cpp; exit
#endif

#ifndef BOOST_UNITS_COMPARISON
#define BOOST_UNITS_COMPARISON
#include<boost/units/quantity.hpp>
#include<boost/units/is_quantity.hpp>
#include<type_traits> // std::is_constructible

namespace boost{
namespace units{

namespace{
	typedef void (*unspecified_null_pointer_constant_for_comparison_type)(int*******);
}

#define DECLARE_ZERO_COMPARISON(OperatoRSymboL) \
template<class U1, class Q2, typename = typename std::enable_if<not std::is_same<quantity<U1>, Q2>::value>::type> \
auto operator OperatoRSymboL(quantity<U1> const& q1, Q2 const& q2) -> decltype(q1 OperatoRSymboL quantity<U1>(q2)){ \
	return q1 OperatoRSymboL quantity<U1>(q2); \
} \
template<class U1, class T1> \
bool operator OperatoRSymboL(quantity<U1, T1> const& q1, unspecified_null_pointer_constant_for_comparison_type /*q2*/){ \
	return q1 OperatoRSymboL quantity<U1, T1>(0); \
}
// this generates operator> , operator<, operator==, operator>=, operator <= 
// ... for mixed units, and for "literal" zeros

DECLARE_ZERO_COMPARISON(>)
DECLARE_ZERO_COMPARISON(<)
DECLARE_ZERO_COMPARISON(>=)
DECLARE_ZERO_COMPARISON(<=)
DECLARE_ZERO_COMPARISON(==)

#undef DECLARE_ZERO_COMPARISON

template<class U, class K> 
quantity<U, K> min(
	quantity<U, K> const& q, 
	unspecified_null_pointer_constant_for_comparison_type
){
	using std::min;
	return min(q, quantity<U, K>(0));
}
template<class U, class K> 
quantity<U, K> min(
	unspecified_null_pointer_constant_for_comparison_type, 
	quantity<U, K> const& q
){
	using std::min;
	return min(quantity<U, K>(0), q);
}

template<class U, class K> 
quantity<U, K> max(
	quantity<U, K> const& q, 
	unspecified_null_pointer_constant_for_comparison_type
){
	using std::max;
	return max(q, boost::units::quantity<U, K>(0));
}

template<class U, class K> 
quantity<U, K> max(
	unspecified_null_pointer_constant_for_comparison_type, 
	quantity<U, K> const& q
){
	using std::max;
	return max(quantity<U, K>(0), q);
}

}}


#ifdef _TEST_BOOST_UNITS_COMPARISON

#include<boost/units/systems/si/io.hpp>
#include<boost/units/systems/cgs/io.hpp>

#include<boost/units/cmath.hpp>
#include<boost/numeric/interval.hpp>

#include<cassert>
#include<iostream>

using std::clog;
using namespace boost::units;

int main(){
	quantity<si::length> q = 23.*si::meter;
	assert(q > 0);
	quantity<si::temperature> t = 464.*si::kelvin;
	assert(t > 0);
	quantity<si::temperature> t2 = -464.*si::kelvin;
	assert(t2 < 0);
	quantity<si::temperature> tz = 0.*si::kelvin;
	tz*=10.;
	tz/=10.;
	assert(tz == 0);
	boost::numeric::interval<float> ft;
	boost::numeric::interval<double> id;

	assert( t > 0);
	assert( min(t, 0) == 0);
	assert( min(0, t) == 0);

	std::clog << quantity<cgs::length>() << std::endl;
	std::clog << quantity<si::length>() << std::endl;
	std::clog << quantity<cgs::length>(quantity<si::length>(101.*cgs::centimeter)) << std::endl;
	assert(101.*cgs::centimeter > 1.*si::meter);
}
#endif
#endif

