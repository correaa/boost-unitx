#ifdef compile_instructions
ln -sf $0 $0.cpp && c++ -std=c++0x $0.cpp -Wfatal-errors -D_TEST_BOOST_UNITS_SI_ABBREVIATIONS -o $0.x && $0.x $@; rm -f $0.cpp $0.x; exit
#endif

#ifndef BOOST_UNITS_SI_ABBREVIATIONS_HPP
#define BOOST_UNITS_SI_ABBREVIATIONS_HPP

#include<boost/units/quantity.hpp>
#include<boost/units/systems/si.hpp>
#include<boost/units/systems/si/codata_constants.hpp>
#include<boost/units/systems/si/prefixes.hpp>
#include<boost/units/systems/si/amount.hpp>
#include<boost/units/systems/si/codata/typedefs.hpp>

#include<boost/units/systems/si/luminous_intensity.hpp>
#include<boost/units/systems/si/luminous_flux.hpp>
#include<boost/units/systems/si/pressure.hpp>
#include<boost/units/systems/si/inductance.hpp>
#include<boost/units/systems/si/illuminance.hpp>
#include<boost/units/systems/si/activity.hpp>
#include<boost/units/systems/si/absorbed_dose.hpp>
#include<boost/units/systems/si/dose_equivalent.hpp>
#include<boost/units/systems/si/catalytic_activity.hpp>
#include<boost/units/systems/si/action.hpp>
#include<boost/units/systems/si/plane_angle.hpp>

#include<boost/units/cmath.hpp>

namespace boost{
namespace units{
namespace si{

// abbreviations with atmost three letters are documented here 
// http://physics.nist.gov/cuu/Units/units.html
namespace abbreviations{

//base units
	static const length             m   = si::meter; 
	static const mass               kg  = si::kilogram;
	static const time               s   = si::second;
	static const current            A   = si::ampere;
	static const temperature        K   = si::kelvin;
	static const amount             mol = si::mole;
	static const luminous_intensity cd  = si::candela;
	static const plane_angle        rad = si::radian;
	static const solid_angle        sr  = si::steradian;
//	static const auto               h   = si::hour;
	static const dimensionless      uno; // = si::dimensionless();
	static const dimensionless      U; // = si::dimensionless();

// simple power abbreviations
	static const 
		power_typeof_helper<time, static_rational<2> >::type 
		// auto // good in c++0x
		                s2 = pow<2>(s)
	; 
	static const area   m2 = pow<2>(si::meter); 
	static const volume m3 = pow<3>(si::meter); 
	static const frequency Hz  = si::hertz;
	static const pressure                  Pa  = si::pascal;
	static const auto                     GPa  = si::giga*si::pascal;
	static const /*electric_*/ resistance  Ohm = si::ohm,        Omega = si::ohm;
	using constants::codata::hbar;
//	static const quantity<action> 
//		hbar = boost::units::si::constants::codata::hbar;
	using constants::codata::k_B;
//	static const quantity<constants::codata::energy_over_temperature> 
//		k_B  = boost::units::si::constants::codata::k_B;
	static const quantity<constants::codata::inverse_amount> 
		N_A  = boost::units::si::constants::codata::N_A;
	static const quantity<amount> 
		molec = 1.*mol/(6.0221412927e23);
	static const quantity<mass> 
		m_e  = boost::units::si::constants::codata::m_e;
	static const quantity<velocity>
		c = boost::units::si::constants::codata::c;

//namespace extended{
// derived units
	static const force                     N   = si::newton;
	static const energy                    J   = si::joule;
	static const power                     W   = si::watt;
	static const electric_charge           C   = si::coulomb;
	static const electric_potential        V   = si::volt;
	static const capacitance               F   = si::farad;
	static const /*electric_*/ conductance S   = si::siemens;
	static const magnetic_flux             Wb  = si::weber;
	static const magnetic_flux_density     T   = si::tesla;
	static const inductance                H   = si::henry;
	static const luminous_flux             lm  = si::lumen;
	static const illuminance               lx  = si::lux;
	static const activity                  Bq  = si::becquerel;
	static const absorbed_dose             Gy  = si::gray;
	static const dose_equivalent           Sv  = si::sievert;
	static const catalytic_activity        kat = si::katal;

// constants, it is good (for phoenix and others) to have constants as quantities and not as "units.constants" with errors 
	static const quantity<action>                                            h    = boost::units::si::constants::codata::h;
	static const quantity<constants::codata::energy_over_temperature_amount> R    = boost::units::si::constants::codata::R;

// abbreviations with prefixes
//}

namespace prefixes{
//prefixes http://physics.nist.gov/cuu/Units/prefixes.html
	static const yotta_type Y;
	static const zetta_type Z;
	static const exa_type   E;
	static const peta_type  P;
	static const tera_type  T;  // conflict with T(Testa)
	static const giga_type  G;
	static const mega_type  M;
	static const kilo_type  k;
	static const hecto_type h;
	static const deka_type  da;
	static const deci_type  d;
	static const centi_type c;
	static const milli_type m;  // conflict with m(meter)
	static const micro_type mu, u; //µ
	static const nano_type  n;
	static const pico_type  p;
	static const femto_type f;
	static const atto_type  a;
	static const zepto_type z;
	static const yocto_type y;
}

//common prefix/unit combination
// needs c++0x
	static const //auto             
		unit<list<dim<length_base_dimension, static_rational<1l, 1l> >, dimensionless_type>, heterogeneous_system<heterogeneous_system_impl<list<heterogeneous_system_dim<si::meter_base_unit, static_rational<1l, 1l> >, dimensionless_type>, list<dim<length_base_dimension, static_rational<1l, 1l> >, dimensionless_type>, list<scale_list_dim<scale<10l, static_rational<-2l, 1l> > >, dimensionless_type> > >, void>
		cm   = si::centi*si::meter
	;
	static const //auto             
		unit<list<dim<length_base_dimension, static_rational<1l, 1l> >, dimensionless_type>, heterogeneous_system<heterogeneous_system_impl<list<heterogeneous_system_dim<si::meter_base_unit, static_rational<1l, 1l> >, dimensionless_type>, list<dim<length_base_dimension, static_rational<1l, 1l> >, dimensionless_type>, list<scale_list_dim<scale<10l, static_rational<-3l, 1l> > >, dimensionless_type> > >, void>
		mm   = si::milli*si::meter
	;
	static const //auto             
		unit<list<dim<length_base_dimension, static_rational<1l, 1l> >, dimensionless_type>, heterogeneous_system<heterogeneous_system_impl<list<heterogeneous_system_dim<si::meter_base_unit, static_rational<1l, 1l> >, dimensionless_type>, list<dim<length_base_dimension, static_rational<1l, 1l> >, dimensionless_type>, list<scale_list_dim<scale<10l, static_rational<-9l, 1l> > >, dimensionless_type> > >, void>
		nm   = si::nano*si::meter
	;
	static const //auto_conversion_operators
		unit<list<dim<time_base_dimension, static_rational<1l, 1l> >, dimensionless_type>, heterogeneous_system<heterogeneous_system_impl<list<heterogeneous_system_dim<si::second_base_unit, static_rational<1l, 1l> >, dimensionless_type>, list<dim<time_base_dimension, static_rational<1l, 1l> >, dimensionless_type>, list<scale_list_dim<scale<10l, static_rational<-15l, 1l> > >, dimensionless_type> > >, void>
		fs   = si::femto*si::second
	;

	

	/*
	static const auto             cm   = si::centi*si::meter;
	static const auto             km   = si::kilo*si::meter;

	static const auto              g   = si::milli*si::kilogram;

	static const auto             ms   = si::milli*si::second;
	static const auto             ns   = si::nano*si::second;
	static const auto             fs   = si::femto*si::second;

	static const auto             mA   = si::milli*si::ampere;
	static const auto             kW   = si::kilo*si::watt;

	static const auto             mJ   = si::milli*si::joule;
	static const auto             MJ   = si::mega*si::joule;
	*/
}

}}}
#ifndef NO_AUTO_NAMESPACE
namespace global{
	using namespace boost::units::si::abbreviations;
}
#endif

#ifdef _TEST_BOOST_UNITS_SI_ABBREVIATIONS

#include<boost/units/systems/si/io.hpp>
#include<boost/units/base_units/metric/hour.hpp>
#include<boost/units/detail/utility.hpp> //not necessary if boost.units is already included
using boost::units::detail::demangle;

using std::cout; 
using std::endl;

int main(){
	using namespace boost::units;
	using namespace si::abbreviations;
// base units
	cout 
		<< 2.*m   << endl
		<< 1.*kg  << endl
		<< 1.*s   << endl
		<< 1.*A   << endl
		<< 1.*K   << endl
		<< 1.*mol << endl
		<< 1.*cd  << endl
		<< 1.*rad << endl
		<< 1.*sr  << endl
	;
// simple powers
	cout 
		<< 2.*m/s   << endl
		<< 4.*m/s2  << endl
		<< 1.*kg/m3 << endl
	;
// derived units
//	using namespace si::abbreviations::extended;
	cout
		<< 1.*Hz  << endl
		<< 1.*N   << endl
		<< 1.*Pa  << endl
		<< 1.*J   << endl
		<< 1.*W   << endl
		<< 1.*C   << endl
		<< 1.*V   << endl
		<< 1.*F   << endl
		<< 1.*Ohm << " " << 1.*Omega << endl
		<< 1.*S   << endl
		<< 1.*Wb  << endl
		<< 1.*T   << endl
		<< 1.*H   << endl
		<< 1.*lm  << endl
		<< 1.*lx  << endl
		<< 1.*Bq  << endl
		<< 1.*Gy  << endl
		<< 1.*Sv  << endl
		<< 1.*kat << endl
	;
// prefixed
	{
		using namespace boost::units::si::abbreviations::prefixes;
		using boost::units::si::abbreviations::m;
		cout
			<< 1.*k*m << endl
		;
	}
// prefix/unit combination
	cout
		<< 1.*mm << endl
		<< 1.*cm << endl
		//<< 1.*km << endl
		//<< 1.*kW << endl
	;
	cout << hbar << " " << k_B << endl;
	std::clog << demangle(typeid(mm).name()) << std::endl;

	cout << quantity<si::velocity>(4.*nm/fs) << std::endl;
	cout << 0.0150*fs << std::endl;
	
	std::istringstream iss("4. s");
	quantity<si::time> t; 
	///iss >> t;
	cout << t;

	{
		std::istringstream iss("15. s");
		decltype(15.*fs) dt; 
		//iss >> dt;
		cout << dt << endl;
	}
	cout << 5.*uno << endl;
	cout << 1.*c << endl;
	return 0;
}
#endif

#endif

// Local variables:
// eval:(setq c-basic-offset 4 tab-width 4 indent-tabs-mode t truncate-lines 1 substatement-open 0 statement-case-open + inline-open 0 arglist-cont-nonempty (c-indent-operator-lineup-arglist-operators c-lineup-arglist) )
// End:
/* vim:set ft=cpp ts=4 sw=4 sts=4 nowrap: cindent: */

