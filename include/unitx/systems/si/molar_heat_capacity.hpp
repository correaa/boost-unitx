// Boost.Units - A C++ library for zero-overhead dimensional analysis and 
// unit/quantity manipulation and conversion
//
// Copyright (C) 2003-2008 Matthias Christian Schabel
// Copyright (C) 2008 Steven Watanabe
// Copyright (C) 2011 Alfredo Correa
//
// Distributed under the Boost Software License, Version 1.0. (See
// accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#ifndef BOOST_UNITS_SI_MOLAR_HEAT_CAPACITY_HPP
#define BOOST_UNITS_SI_MOLAR_HEAT_CAPACITY_HPP

#include <boost/units/systems/si/base.hpp>
#include <boost/units/physical_dimensions/molar_heat_capacity.hpp>

namespace boost {

namespace units { 

namespace si {

typedef 
	unit<molar_heat_capacity_dimension, boost::units::si::system>  
	molar_heat_capacity
;

//typedef 
//	unit<molar_heat_capacity_dimension, boost::units::si::system>  
//	molar_entropy
//;


BOOST_UNITS_STATIC_CONSTANT(joule_per_kelvin_per_mole , molar_heat_capacity); //reciprocal label is consistent with si/velocity.hpp
BOOST_UNITS_STATIC_CONSTANT(joules_per_kelvin_per_mol , molar_heat_capacity);
BOOST_UNITS_STATIC_CONSTANT(joule_per_kelvin_per_mol  , molar_heat_capacity); //reciprocal label is consistent with si/velocity.hpp
BOOST_UNITS_STATIC_CONSTANT(joules_per_kelvin_per_mole, molar_heat_capacity);

inline std::string name_string(const reduce_unit<si::molar_heat_capacity>::type&)   { 
	return 
//		"SI unit of molar heat capacity"; 
		"joule per mole per kelvin";
}
inline std::string symbol_string(const reduce_unit<si::molar_heat_capacity>::type&) { return "J/mol/K"; /*"G"*/ }

} // namespace si

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_SI_HEAT_CAPACITY_HPP

