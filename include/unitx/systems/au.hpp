// Copyright (C) 2018 Alfredo A. Correa

#ifndef BOOST_UNITS_AU_HPP
#define BOOST_UNITS_AU_HPP

#ifndef BOOST_UNITS_AU_USE_BASE_COULOMB_FORCE_CONSTANT
#define BOOST_UNITS_AU_USE_BASE_COULOMB_FORCE_CONSTANT
//#define BOOST_UNITS_AU_USE_BASE_BOHR_RADIUS
//#define BOOST_UNITS_AU_USE_BASE_HARTREE // doesn't work?
#endif

#include "unitx/systems/au/base.hpp"

#include "unitx/systems/au/mass.hpp"
#include "unitx/systems/au/electric_charge.hpp"
#include "unitx/systems/au/action.hpp"
#include "unitx/systems/au/elastivity.hpp"
#include "unitx/systems/au/heat_capacity.hpp"
#include "unitx/systems/au/amount.hpp"
#include "unitx/systems/au/energy.hpp"

#include "unitx/systems/au/frequency.hpp"
#include "unitx/systems/au/length.hpp"
#include "unitx/systems/au/time.hpp"
#include "unitx/systems/au/velocity.hpp"
#include "unitx/systems/au/force.hpp"
#include "unitx/systems/au/temperature.hpp"
#include "unitx/systems/au/pressure.hpp"
#include "unitx/systems/au/volume.hpp"
#include "unitx/systems/au/inverse_volume.hpp"

#include "unitx/systems/au/molar_heat_capacity.hpp"
#include "unitx/systems/au/molar_energy.hpp"
#include "unitx/systems/au/molar_volume.hpp"
#include "unitx/systems/au/molarity.hpp"

#include "unitx/systems/au/area.hpp"
#include "unitx/systems/au/inverse_area.hpp"

#include "unitx/systems/au/wavenumber.hpp"

#include "unitx/systems/au/plane_angle.hpp"
#include "unitx/systems/au/mass_flow.hpp"

#include <boost/units/quantity.hpp>

#include <boost/units/base_units/angle/radian.hpp>
#include <boost/units/base_units/angle/steradian.hpp>
#endif
