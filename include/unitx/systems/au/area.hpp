#ifndef BOOST_UNITS_AU_AREA_HPP
#define BOOST_UNITS_AU_AREA_HPP

#include "../../systems/au/base.hpp"
#include<boost/units/physical_dimensions/area.hpp>

namespace boost {

namespace units { 

namespace au {

typedef unit<

	derived_dimension<length_base_dimension,2>::type,
	au::system>      area;
    
} // namespace au

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_AU_AREA_HPP

