#ifndef BOOST_UNITS_AU_VOLUME_HPP
#define BOOST_UNITS_AU_VOLUME_HPP

#include "../../systems/au/base.hpp"
#include<boost/units/physical_dimensions/volume.hpp>

namespace boost {

namespace units { 

namespace au {

typedef unit<

	derived_dimension<length_base_dimension,3>::type,
	au::system>      volume;
    
} // namespace au

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_AU_VOLUME_HPP

