#ifdef TEST_INSTRUCTIONS
sh ../../../units/test/au_si_conversion.cpp; exit
#endif

#ifndef BOOST_UNITS_SYSTEMS_AU_SI_CONVERSION
#define BOOST_UNITS_SYSTEMS_AU_SI_CONVERSION

#include "../../systems/au.hpp"
#include<boost/units/systems/si.hpp>

#include<boost/units/systems/si/codata/electron_constants.hpp> // codata::m_e
#include<boost/units/systems/si/codata/electromagnetic_constants.hpp> // codata::e
#include<boost/units/systems/si/codata/universal_constants.hpp> // codata::hbar
#include<boost/units/systems/si/codata/physico-chemical_constants.hpp> // codata::k_B

//conversion factors using library codata
/* 1 - mass */
BOOST_UNITS_DEFINE_CONVERSION_FACTOR(
	boost::units::au::electron_mass_base_unit,
	boost::units::si::mass,
	double, boost::units::si::constants::codata::m_e.value().value() // 9.10938215e-31
);
BOOST_UNITS_DEFAULT_CONVERSION(
	boost::units::au::electron_mass_base_unit,
	boost::units::si::mass
);

/* 2 - electric charge */
typedef boost::units::unit<
		boost::units::electric_charge_dimension, 
		boost::units::si::system
	> electrinc_charge_si_unit;
BOOST_UNITS_DEFINE_CONVERSION_FACTOR(
	boost::units::au::elementary_charge_base_unit,
	electrinc_charge_si_unit,
	double, boost::units::si::constants::codata::e.value().value() // 1.60217653e-19 
);
BOOST_UNITS_DEFAULT_CONVERSION(
	boost::units::au::elementary_charge_base_unit,
	electrinc_charge_si_unit
);

/* 3 - action */
typedef boost::units::si::action si_action;
BOOST_UNITS_DEFINE_CONVERSION_FACTOR(
	boost::units::au::reduced_planck_constant_base_unit,
	si_action,
	double, 
	boost::units::si::constants::codata::hbar.value().value() // 1.05457168e-34
);
BOOST_UNITS_DEFAULT_CONVERSION(
	boost::units::au::reduced_planck_constant_base_unit,
        si_action
);

/* 4 - electric constant */
typedef boost::units::unit<
		boost::units::derived_dimension<
			boost::units::length_base_dimension,   3,
			boost::units::mass_base_dimension,     1,
			boost::units::time_base_dimension,    -4,
			boost::units::current_base_dimension, -2
		>::type,
		boost::units::si::system
	> electric_constant_si_unit;
BOOST_UNITS_DEFINE_CONVERSION_FACTOR(
	boost::units::au::coulomb_force_constant_base_unit,
	electric_constant_si_unit,
	double, 
	1./(4.*M_PI*boost::units::si::constants::codata::epsilon_0.value().value()) /// 8.9875517873681e9 
	/* https://en.wikipedia.org/wiki/Atomic_units#Fundamental_atomic_units */
);
BOOST_UNITS_DEFAULT_CONVERSION(
	boost::units::au::coulomb_force_constant_base_unit,
	electric_constant_si_unit
);

/* 5 - boltzman_constant */
typedef 
	boost::units::unit<
		boost::units::heat_capacity_dimension, 
		boost::units::si::system
	> 
	boltzman_constant_si_unit
;
BOOST_UNITS_DEFINE_CONVERSION_FACTOR(
	boost::units::au::boltzmann_constant_base_unit,
	boltzman_constant_si_unit,
	double, 
	boost::units::si::constants::codata::k_B.value().value() // 1.3806504e-23
);
BOOST_UNITS_DEFAULT_CONVERSION(
	boost::units::au::boltzmann_constant_base_unit,
	boltzman_constant_si_unit
);

/* 6 - amount */
BOOST_UNITS_DEFINE_CONVERSION_FACTOR(
	boost::units::au::species_base_unit,
	boost::units::si::amount,
	double, 
	1./boost::units::si::constants::codata::N_A.value().value() // 1./6.02214179e23
);
BOOST_UNITS_DEFAULT_CONVERSION(
	boost::units::au::species_base_unit,
	boost::units::si::amount
);

/*
BOOST_UNITS_DEFINE_CONVERSION_FACTOR(
	boost::units::si::temperature,
	boost::units::atomic::temperature,
	double, 22.
);
BOOST_UNITS_DEFAULT_CONVERSION(
	boost::units::si::temperature,
	boost::units::atomic::temperature
);*/

/*
BOOST_UNITS_DEFINE_CONVERSION_FACTOR(
	boost::units::atomic::energy,
	boost::units::si::energy,
	double, si::constants::codata::E_h.value().value() //4.35974394e-18
);
BOOST_UNITS_DEFAULT_CONVERSION(
	boost::units::atomic::energy,
	boost::units::si::energy
);*/


/* commented because hartree is not a base unit
BOOST_UNITS_DEFINE_CONVERSION_FACTOR(
//	atomic::hartree_energy_base_unit,
	atomic::energy_unit,
	si::energy,
	double, si::constants::codata::E_h.value().value() //4.35974394e-18
);
BOOST_UNITS_DEFAULT_CONVERSION(
//	atomic::hartree_energy_base_unit,
	atomic::energy_unit,
	si::energy
);
*/
/*
BOOST_UNITS_DEFINE_CONVERSION_FACTOR(
	atomic::boltzman_constant_base_unit,
	(unit<
		derived_dimension<
			length_base_dimension, 2,
			mass_base_dimension, 1,
			time_base_dimension, -2, // == ,energy_dimension, 1,
			temperature_base_dimension, -1
		>::type,
		si::system
	>),
	double, si::constants::codata::k_B.value().value() //1.3806504e-23
);
BOOST_UNITS_DEFAULT_CONVERSION(
	atomic::boltzman_constant_base_unit,
	(unit<
		derived_dimension<
			length_base_dimension, 2,
			mass_base_dimension, 1,
			time_base_dimension, -2, // == ,energy_dimension, 1,
			temperature_base_dimension, -1
		>::type,
		si::system
	>)
);
*/

#endif

