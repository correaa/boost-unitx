#ifndef BOOST_UNITS_AU_AMOUNT_HPP
#define BOOST_UNITS_AU_AMOUNT_HPP

#include "../../systems/au/base.hpp"

namespace boost {

namespace units { 

namespace au {

typedef unit<amount_dimension,au::system> amount;

BOOST_UNITS_STATIC_CONSTANT(molecule,amount);
BOOST_UNITS_STATIC_CONSTANT(molecules,amount);
BOOST_UNITS_STATIC_CONSTANT(atom,amount);
BOOST_UNITS_STATIC_CONSTANT(atoms,amount);
BOOST_UNITS_STATIC_CONSTANT(species,amount);

} // namespace au

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_AU_AMOUNT_HPP

