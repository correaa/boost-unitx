#ifndef BOOST_UNITS_AU_MOLARITY_HPP
#define BOOST_UNITS_AU_MOLARITY_HPP

#include "../../systems/au/base.hpp"
#include "../../physical_dimensions/molarity.hpp"

namespace boost {

namespace units { 

namespace au {

typedef unit<

	derived_dimension<length_base_dimension,-3,
                          amount_base_dimension,1>::type,
	au::system>      molarity;

typedef molarity number_density;

} // namespace au

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_AU_MOLARITY_HPP

