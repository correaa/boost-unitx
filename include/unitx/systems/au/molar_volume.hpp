#ifndef BOOST_UNITS_AU_MOLAR_VOLUME_HPP
#define BOOST_UNITS_AU_MOLAR_VOLUME_HPP

#include "../../systems/au/base.hpp"
#include<boost/units/physical_dimensions/amount.hpp>
//#include "alf/boost/units/physical_dimensions/amount_atom.hpp"

namespace boost {

namespace units { 

namespace au {

typedef unit<

	derived_dimension<length_base_dimension,3,
                          amount_base_dimension,-1>::type,
	au::system>      molar_volume;
    
} // namespace au

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_AU_MOLAR_VOLUME_HPP

