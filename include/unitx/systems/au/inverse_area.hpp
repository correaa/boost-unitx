#ifndef BOOST_UNITS_AU_INVERSE_AREA_HPP
#define BOOST_UNITS_AU_INVERSE_AREA_HPP

#include "../../systems/au/base.hpp"
#include "../../physical_dimensions/inverse_area.hpp"

namespace boost {

namespace units { 

namespace au {

typedef unit<

	derived_dimension<length_base_dimension,-2>::type,
	au::system>      inverse_area;
    
} // namespace au

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_AU_INVERSE_AREA_HPP

