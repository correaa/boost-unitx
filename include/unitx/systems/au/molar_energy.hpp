#ifndef BOOST_UNITS_AU_MOLAR_ENERGY_HPP
#define BOOST_UNITS_AU_MOLAR_ENERGY_HPP

#include "../../systems/au/base.hpp"
#include<boost/units/physical_dimensions/amount.hpp>

namespace boost {

namespace units { 

namespace au {

typedef unit<

	derived_dimension<length_base_dimension,2,
                          mass_base_dimension,1,
                          time_base_dimension,-2,
                          temperature_base_dimension,0,
                          amount_base_dimension,-1>::type,
	au::system>      molar_energy;
    
} // namespace au

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_AU_MOLAR_ENERGY_HPP

