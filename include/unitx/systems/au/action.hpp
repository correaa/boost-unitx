#ifndef BOOST_UNITS_AU_ACTION_HPP
#define BOOST_UNITS_AU_ACTION_HPP

#include "../../systems/au/base.hpp"

namespace boost {

namespace units { 

namespace au {

typedef unit<action_dimension,au::system>      action;
    
BOOST_UNITS_STATIC_CONSTANT(reduced_planck_constant,action); 

} // namespace au

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_AU_ELECTRIC_CHARGE_HPP

