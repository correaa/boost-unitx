#ifndef BOOST_UNITS_AU_FORCE_HPP
#define BOOST_UNITS_AU_FORCE_HPP

#include "../../systems/au/base.hpp"
#include <boost/units/physical_dimensions/force.hpp>

namespace boost {

namespace units { 

namespace au {

typedef unit<force_dimension,au::system>      force;
    
BOOST_UNITS_STATIC_CONSTANT(au_of_force,force); 

} // namespace au

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_AU_FORCE_HPP

