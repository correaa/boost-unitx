#ifndef BOOST_UNITS_AU_TEMPERATURE_HPP
#define BOOST_UNITS_AU_TEMPERATURE_HPP

#include "../../systems/au/base.hpp"
#include <boost/units/physical_dimensions/temperature.hpp>

namespace boost {

namespace units { 

namespace au {

typedef unit<temperature_dimension,au::system>      temperature;
    
BOOST_UNITS_STATIC_CONSTANT(au_of_temperature,temperature); 

} // namespace au

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_AU_TEMPERATURE_HPP

