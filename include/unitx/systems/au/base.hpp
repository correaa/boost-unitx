#ifndef BOOST_UNITS_AU_BASE_HPP
#define BOOST_UNITS_AU_BASE_HPP

#include <string>

#include <boost/units/static_constant.hpp>
#include <boost/units/unit.hpp>
#include <boost/units/make_system.hpp>

#include "../../base_units/au/boltzmann_constant.hpp"
#include "../../base_units/au/species.hpp"
#include "../../base_units/au/electron_mass.hpp"
#include "../../base_units/au/elementary_charge.hpp"
#include "../../base_units/au/reduced_planck_constant.hpp"
#include "../../base_units/au/coulomb_force_constant.hpp"

#include <boost/units/base_units/angle/radian.hpp>
#include <boost/units/base_units/angle/steradian.hpp>

namespace boost{

namespace units{

namespace au{

typedef make_system<
    angle::radian_base_unit,
	boltzmann_constant_base_unit,
		            species_base_unit,
					electron_mass_base_unit,
                    elementary_charge_base_unit,
                    reduced_planck_constant_base_unit,
//#if defined   BOOST_UNITS_AU_USE_BASE_COULOMB_FORCE_CONSTANT
                    coulomb_force_constant_base_unit
//#elif defined  BOOST_UNITS_AU_USE_BASE_BOHR_RADIUS
//					bohr_radius_base_unit
//#elif defined  BOOST_UNITS_AU_USE_BASE_HARTREE
//					hartree_energy_base_unit
//#endif
>::type system;

/// dimensionless au unit
typedef unit<dimensionless_type,system>         dimensionless;

BOOST_UNITS_STATIC_CONSTANT(uno,dimensionless);

}

}

}

#endif

