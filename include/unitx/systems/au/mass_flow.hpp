#ifndef BOOST_UNITS_AU_MASS_FLOW_HPP
#define BOOST_UNITS_AU_MASS_FLOW_HPP

#include "../../physical_dimensions/mass_flow.hpp"

namespace boost {

namespace units { 

namespace au {

typedef unit<

	derived_dimension<mass_base_dimension,1,
                          time_base_dimension,-1>::type,
	au::system>      mass_flow;
    
} // namespace au

} // namespace units

} // namespace boost

#endif

