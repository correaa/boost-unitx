#ifndef BOOST_UNITS_AU_MASS_HPP
#define BOOST_UNITS_AU_MASS_HPP

#include "../../systems/au/base.hpp"

namespace boost {

namespace units { 

namespace au {

typedef unit<mass_dimension,au::system>      mass;
    
BOOST_UNITS_STATIC_CONSTANT(electron_rest_mass,mass); 
BOOST_UNITS_STATIC_CONSTANT(electron_mass,mass);

} // namespace au

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_AU_MASS_HPP

