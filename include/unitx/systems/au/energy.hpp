#ifndef BOOST_UNITS_AU_ENERGY_HPP
#define BOOST_UNITS_AU_ENERGY_HPP

#include "../../systems/au/base.hpp"
#include <boost/units/physical_dimensions/energy.hpp>

namespace boost {

namespace units { 

namespace au {

typedef unit<energy_dimension,au::system>      energy;
    
BOOST_UNITS_STATIC_CONSTANT(hartree_energy,energy); 
BOOST_UNITS_STATIC_CONSTANT(hartree,energy);

} // namespace au

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_AU_ENERGY_HPP

