#ifndef BOOST_UNITS_AU_ELASTIVITY_HPP
#define BOOST_UNITS_AU_ELASTIVITY_HPP

#include "../../systems/au/base.hpp"
#include "../../physical_dimensions/elastivity.hpp"

namespace boost {

namespace units { 

namespace au {

typedef unit<elastivity_dimension,au::system> elastivity;
    
BOOST_UNITS_STATIC_CONSTANT(coulomb_force_constant, elastivity); 

} // namespace au

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_AU_ELASTIVITY_HPP

