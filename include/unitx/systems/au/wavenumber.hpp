#ifndef BOOST_UNITS_AU_WAVENUMBER_HPP
#define BOOST_UNITS_AU_WAVENUMBER_HPP

#include "../../systems/au/base.hpp"
#include<boost/units/physical_dimensions/wavenumber.hpp>

namespace boost {

namespace units { 

namespace au {

typedef unit<wavenumber_dimension,au::system>      wavenumber;
    
} // namespace au

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_AU_WAVENUMBER_HPP

