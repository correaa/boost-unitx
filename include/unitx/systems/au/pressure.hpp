#ifndef BOOST_UNITS_AU_PRESSURE_HPP
#define BOOST_UNITS_AU_PRESSURE_HPP

#include "../../systems/au/base.hpp"
#include <boost/units/physical_dimensions/pressure.hpp>

namespace boost {

namespace units { 

namespace au {

typedef unit<pressure_dimension,au::system>      pressure;
    
BOOST_UNITS_STATIC_CONSTANT(au_of_pressure,pressure); 
BOOST_UNITS_STATIC_CONSTANT(pressure_unit,pressure); 

} // namespace au

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_AU_PRESSURE_HPP

