#ifndef BOOST_UNITS_AU_VELOCITY_HPP
#define BOOST_UNITS_AU_VELOCITY_HPP

#include "../../systems/au/base.hpp"
#include <boost/units/physical_dimensions/velocity.hpp>

namespace boost {

namespace units { 

namespace au {

typedef unit<velocity_dimension,au::system>      velocity;
    
BOOST_UNITS_STATIC_CONSTANT(au_of_velocity,velocity); 

} // namespace au

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_AU_VELOCITY_HPP

