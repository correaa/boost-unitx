#ifndef BOOST_UNITS_MOLARITY_DERIVED_DIMENSION_HPP
#define BOOST_UNITS_MOLARITY_DERIVED_DIMENSION_HPP

#include <boost/units/derived_dimension.hpp>
#include <boost/units/physical_dimensions/length.hpp>

namespace boost {

namespace units {

/// derived dimension for molarity : n L^-3
typedef 
	derived_dimension<
		length_base_dimension,-3,
		amount_base_dimension, 1
	>::type molarity_dimension
;

typedef molarity_dimension number_density_dimension;

} // namespace units

} // namespace boost

#endif

