#ifdef compile_instructions
(echo "#include\""$0"\"" > $0x.cpp) && time c++ -std=c++11 -Wfatal-errors -D_TEST_BOOST_UNITS_PHYSICAL_DIMENSIONS_IO_HPP $0x.cpp -o $0x.x -lstdc++fs && $0x.x $@ && (xdg-open ${0%.*}.pdf 2>/dev/null &); rm -rf $0x.cpp $0x.x; exit
#endif

#ifndef ALF_BOOST_UNITS_PHYSICAL_DIMENSIONS_IO_HPP
#define ALF_BOOST_UNITS_PHYSICAL_DIMENSIONS_IO_HPP

//#include<typeinfo>

#include "../../units/physical_dimensions.hpp"
//#include<boost/units/detail/utility.hpp>
//#include<boost/regex.hpp>

#include<string>

namespace boost{
namespace units{

template<class Dimension> std::string name                   (){
	return "Quantity"; // don't try to deduce the name of the Dimension from the type as they are usually typedefs
};

// Capitalized name of physical dimensions, note that this is independent of unit system

template<> std::string name<dimensionless_type              >(){return "Dimensionless";}
template<> std::string name<                action_dimension>(){return "Action";}
template<> std::string name<                energy_dimension>(){return "Energy";}
template<> std::string name<                 force_dimension>(){return "Force";}
template<> std::string name<             frequency_dimension>(){return "Frequency";}
template<> std::string name<              velocity_dimension>(){return "Velocity";}
template<> std::string name<                volume_dimension>(){return "Volume";}
template<> std::string name<                  area_dimension>(){return "Area";}
template<> std::string name<          inverse_area_dimension>(){return "Inverse Area";}
template<> std::string name<       specific_volume_dimension>(){return "Specific Volume";}
template<> std::string name<                length_dimension>(){return "Length";}
template<> std::string name<              pressure_dimension>(){return "Pressure";}
template<> std::string name<           temperature_dimension>(){return "Temperature";}
template<> std::string name<         heat_capacity_dimension>(){return "Entropy";}
template<> std::string name<specific_heat_capacity_dimension>(){return "Specific Entropy";}
template<> std::string name<                  time_dimension>(){return "Time";}
template<> std::string name<                  mass_dimension>(){return "Mass";}
template<> std::string name<          mass_density_dimension>(){return "Mass Density";}
template<> std::string name<  thermal_conductivity_dimension>(){return "Thermal Conductivity";}
template<> std::string name<            wavenumber_dimension>(){return "Wavenumber";}
template<> std::string name<       specific_energy_dimension>(){return "Specific Energy";}
template<> std::string name<                amount_dimension>(){return "Amount";}
template<> std::string name<          molar_energy_dimension>(){return "Molar Energy";}
template<> std::string name<          molar_volume_dimension>(){return "Molar Volume";}
template<> std::string name<   molar_heat_capacity_dimension>(){return "Molar Heat Capacity";}
template<> std::string name<              molarity_dimension>(){return "Molarity";}
template<> std::string name<        inverse_energy_dimension>(){return "Inverse Energy";}
template<> std::string name<   inverse_temperature_dimension>(){return "Inverse Temperature";}
template<> std::string name<           resistivity_dimension>(){return "Resistivity";}
template<> std::string name<          conductivity_dimension>(){return "Conductivity";}
template<> std::string name<       current_density_dimension>(){return "Current Density";}
template<> std::string name<             mass_flow_dimension>(){return "Mass Flow";}
															
// conductivity in au (((reduced planck constant)^(-3))*(elementary charge)^4*(electron mass)*(coulomb force constant))

//now derived_dimensions, must be defined in terms of *_base_dimensions (not just other derived_dimensions)
template<> std::string name<
	derived_dimension<
		length_base_dimension,  -3,
		time_base_dimension,     1,
		current_base_dimension,  1
	>::type
>(){return "Charge Density";}

template<> std::string name<
	derived_dimension<
		length_base_dimension,  -3
	>::type
>(){return "Inverse Volume";}

}}

#ifdef _TEST_BOOST_UNITS_PHYSICAL_DIMENSIONS_IO_HPP

#include<iostream>
#include<boost/units/systems/si/io.hpp>

using namespace std;
using namespace boost::units;
int main(){
	quantity<si::length> l(2.*si::meter);
	cout << l << endl;
	cout << si::length() << endl;
	cout << boost::units::name<si::length::dimension_type>() << endl;
	cout << boost::units::name<si::conductivity::dimension_type>() << endl;
}
#endif
#endif

