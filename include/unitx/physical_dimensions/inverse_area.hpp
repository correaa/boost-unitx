// Boost.Units - A C++ library for zero-overhead dimensional analysis and 
// unit/quantity manipulation and conversion
//
// Copyright (C) 2003-2008 Matthias Christian Schabel
// Copyright (C) 2008 Steven Watanabe
//
// Distributed under the Boost Software License, Version 1.0. (See
// accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#ifndef BOOST_UNITS_INVERSE_AREA_DERIVED_DIMENSION_HPP
#define BOOST_UNITS_INVERSE_AREA_DERIVED_DIMENSION_HPP

// name taken from http://www.wolframalpha.com/input/?i=1%2Fmeter^2
// alternative was number area density, but that can be confused with [amount][lengh^-2]

#include <boost/units/derived_dimension.hpp>
#include <boost/units/physical_dimensions/length.hpp>

namespace boost {

namespace units {

/// derived dimension for mass density : L^-2
typedef derived_dimension<length_base_dimension,-2
                         >::type inverse_area_dimension;            

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_INVERSE_AREA_DERIVED_DIMENSION_HPP

