#ifndef BOOST_UNITS_MASS_FLOW_DERIVED_DIMENSION_HPP
#define BOOST_UNITS_MASS_FLOW_DERIVED_DIMENSION_HPP

#include <boost/units/derived_dimension.hpp>
#include <boost/units/physical_dimensions/mass.hpp>
#include <boost/units/physical_dimensions/time.hpp>

namespace boost {
namespace units {

/// derived dimension for mass flow : M T^-1
typedef derived_dimension<mass_base_dimension,1,
                          time_base_dimension,-1>::type
mass_flow_dimension;
                          
//typedef current_density_dimension ...

} // namespace units
} // namespace boost

#endif // BOOST_UNITS_MASS_FLOW_DERIVED_DIMENSION_HPP

