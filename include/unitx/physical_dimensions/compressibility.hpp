// Boost.Units - A C++ library for zero-overhead dimensional analysis and 
// unit/quantity manipulation and conversion
//
// Copyright (C) 2003-2008 Matthias Christian Schabel
// Copyright (C) 2008 Steven Watanabe
//
// Distributed under the Boost Software License, Version 1.0. (See
// accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#ifndef BOOST_UNITS_COMPRESSIBILITY_DERIVED_DIMENSION_HPP
#define BOOST_UNITS_COMPRESSIBILITY_DERIVED_DIMENSION_HPP

// name taken from http://www.wolframalpha.com/input/?i=1.%2F%283.*pascal%29
// [mass]^(-1) [length] [time]^2

#include <boost/units/derived_dimension.hpp>
#include <boost/units/physical_dimensions/mass.hpp>
#include <boost/units/physical_dimensions/length.hpp>
#include <boost/units/physical_dimensions/time.hpp>

namespace boost {

namespace units {

typedef derived_dimension<
	  mass_base_dimension  , -1
	, length_base_dimension, +1
	, time_base_dimension  , +2
>::type compressibility_dimension;            

} // namespace units

} // namespace boost

#endif // BOOST_UNITS_COMPRESSIBILITY_DERIVED_DIMENSION_HPP
