#ifndef BOOST_UNITS_ELASTIVITY_DERIVED_DIMENSION_HPP
#define BOOST_UNITS_ELASTIVITY_DERIVED_DIMENSION_HPP

#include <boost/units/derived_dimension.hpp>
#include <boost/units/physical_dimensions/length.hpp>
#include <boost/units/physical_dimensions/mass.hpp>
#include <boost/units/physical_dimensions/time.hpp>
#include <boost/units/physical_dimensions/current.hpp>

namespace boost {
namespace units {

/// derived dimension for permittivity : L^+3 M^+1 T^-4 I^-2
typedef derived_dimension<length_base_dimension,+3,
                          mass_base_dimension,+1,
                          time_base_dimension,-4,
                          current_base_dimension,-2>::type
elastivity_dimension;
                          
typedef elastivity_dimension inverse_permittivity_dimension;

} // namespace units
} // namespace boost

#endif // BOOST_UNITS_ELASTIVITY_DERIVED_DIMENSION_HPP
