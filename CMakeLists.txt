# cmake-format -i CMakeLists.txt

cmake_minimum_required(VERSION 3.16)

project(
  boost_unitx
  VERSION 1.81
  LANGUAGES CXX)

find_package(Boost REQUIRED COMPONENTS headers)

add_library(boost_unitx INTERFACE)
# add_library(Boost::unitx ALIAS boost_unitx)

target_include_directories(boost_unitx INTERFACE include)

target_link_libraries(boost_unitx INTERFACE Boost::headers)

# this makes this library CMake's FetchContent friendly
# https://www.foonathan.net/2022/06/cmake-fetchcontent/
if(NOT CMAKE_CURRENT_SOURCE_DIR STREQUAL CMAKE_SOURCE_DIR)
  return()
endif()

add_subdirectory(test)
enable_testing()
