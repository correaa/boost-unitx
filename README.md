# Boost.Units extensions

Provides some add-ons to make Boost.Units more practical for certain applications.

## Atomic Units System

```
abbreviations.hpp    inverse_area.hpp         molar_volume.hpp
action.hpp           inverse_volume.hpp       plane_angle.hpp
amount.hpp           io                       pressure.hpp
area.hpp             io.hpp                   si_conversion.hpp
base.hpp             io_old.hp                temperature.hpp
elastivity.hpp       length.hpp               texput.log
electric_charge.hpp  mass_flow.hpp            time.hpp
energy.hpp           mass.hpp                 velocity.hpp
force.hpp            molar_energy.hpp         volume.hpp
frequency.hpp        molar_heat_capacity.hpp  wavenumber.hpp
heat_capacity.hpp    molarity.hpp
```

## SI additions

```
abbreviations.hpp  abbreviations.hpp~  molar_energy.hpp  molar_volume.hpp
```

## Added Physical Dimensions

```
amount_atom.hpp      inverse_energy.hpp       mass_flow.hpp
compressibility.hpp  inverse_pressure.hpp     molarity.hpp
current_density.hpp  inverse_temperature.hpp  molar_mass.hpp
elastivity.hpp       inverse_volume.hpp       molar_volume.hpp
inverse_area.hpp     io.hpp
```

## Adaptor to Boost.Interval

```
#include "alf/boost/units/interval.hpp"
```

Add compatibility layer (type traits) to use with Boost.Interval.

## Implicit Conversions

Boost.Units doesn't support implicit conversion between units, even if that conversion is dimensionally correct and well specified.
All conversion need to be explicit.

This modification opens a way to allow implicit conversions

### Unintrusive mode

The typical need for implicit conversion occurs during calls of (non-templated) functions.

```
void f(quantity<cgs::lenght> l){}

f(1.*cgs::centimeter);
f(1.*si::meter); // error, no implicit conversion
```

The way to allow the second case is to include the special header and modify the argument of the function to take a special quantity in the `implicit` namespace.

```
#include "alf/boost/units/implicit.hpp"
void f(implicit::  quantity<cgs::lenght> l){}
```

### Intrusive mode

Alternatively if one wants implicit to be the default one can do
```
#define BOOST_UNITS_NO_ALLOW_IMPLICIT_CONVERSIONS
#include "alf/boost/units/implicit.hpp"
void f(quantity<cgs::lenght> l){}
```
and all the units conversion between compatible units with well defined conversion factors will become implicit.

This approach has all the downsides of implicit conversions, including surprise conversions, possible loss of precission during conversions, relaxed type safety, etc.
